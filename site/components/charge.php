<?php
require_once('vendor/autoload.php');

// Get the payment token submitted by the form:
$token = $_POST['stripeToken'];
$first_name = filter_var($_POST['first_name'], FILTER_SANITIZE_STRING);
$email = $_POST['email'];

$price = filter_var($_POST['price'], FILTER_SANITIZE_STRING);
$address_line1 = filter_var($_POST['address_line1'], FILTER_SANITIZE_STRING);
$address_line2 = filter_var($_POST['address_line2'], FILTER_SANITIZE_STRING);
$address_city = filter_var($_POST['address_city'], FILTER_SANITIZE_STRING);;
$address_state = filter_var($_POST['address_state'], FILTER_SANITIZE_STRING);
$address_country = filter_var($_POST['address_country'], FILTER_SANITIZE_STRING);
$productName = filter_var($_POST['productName'], FILTER_SANITIZE_STRING);
$priceAdjust = ($price/100);


$address_zip = filter_var($_POST['address_zip'], FILTER_VALIDATE_INT);


// filter_var($str, FILTER_SANITIZE_STRING)

try{
  \Stripe\Stripe::setApiKey("YOUR_SECRET_KEY");

  $customer = \Stripe\Customer::create(array(
    "source" => $token,
    "description" => "Product: " .$productName." Price: $".$price/100,
    "email" => $email,
    "shipping" => array(
      "name" => $first_name,
      "address" => array(
        "line1" => $address_line1,
        "line2" => $address_line2,
        "city" => $address_city,
        "state" => $address_state,
        "country" => $address_country,
        "postal_code" => $address_zip
      )
      )
    )
  );
  // Charge the Customer instead of the card
  \Stripe\Charge::create(array(
      "amount" => $price, // amount in cents, again
      "currency" => "aud",
      "description" => $email." ".$first_name,
      "customer" => $customer->id,
      "shipping" => array(
        "name" => $first_name,
        "address" => array(
          "line1" => $address_line1,
          "line2" => $address_line2,
          "city" => $address_city,
          "state" => $address_state,
          "country" => $address_country,
          "postal_code" => $address_zip
        )
      )
    )
  );

  // Email the lord of land, Moe Hammoud
  mail(
    "moehammoud1@gmail.com","An order has been place by: $first_name","Details: $email,\n".
    "Order Reference: $token\n".
    "Contact Info:\n".
    "Name: $first_name\n".
    "Email: $email\n\n".
    "Order Information: \n".
    "Order / Invoice / Token No : $token\n".
    "Product Name: $productName\n".
    "Price: $price\n".
    "address_line1: $address_line1\n".
    "address_line2: $address_line2\n".
    "address_city: $address_city\n".
    "address_state: $address_state\n".
    "address_country: $address_country\n".
    "address_zip: $address_zip\n"
  );

  // Email the Donator
  mail(
    "$email","Thank you so much for your order, $first_name","Please see your order details below: \n\n".
    "Contact Info:\n".
    "Name: $first_name\n".
    "Email: $email\n\n".
    "Order Information: \n".
    "Order Invoice / Token No : $token\n".
    "Title: $productName\n".
    "Price: $ $priceAdjust inc postage & handling\n".
    "Street Address: $address_line1\n".
    "Street Address 2: $address_line2\n".
    "City: $address_city\n".
    "State: $address_state\n".
    "Country: $address_country\n".
    "Zip: $address_zip\n\n".
    "If there is anything wrong with your order information or you want to ask me any questions, feel free to contact me via moehammoud1@gmail.com\n".
    "Phone: 0403 217 313\n\n".
    "Thank you for donating.\nIf you would like to hear news about my YouTube Tutorials, please let me know."

  );

  echo "success";

} catch (\Stripe\Error\ApiConnection $e) {
    // Network problem, perhaps try again.
    $e_json = $e->getJsonBody();
    $error = $e_json['error'];
    echo "Sorry, your charge couldn't be processed. Reason: ".$error['message'];
} catch (\Stripe\Error\InvalidRequest $e) {
    // You screwed up in your programming. Shouldn't happen!
    $e_json = $e->getJsonBody();
    $error = $e_json['error'];
    echo "Sorry, your charge couldn't be processed. Reason: ".$error['message'];
} catch (\Stripe\Error\Api $e) {
    // Stripe's servers are down!
    $e_json = $e->getJsonBody();
    $error = $e_json['error'];
    echo "Sorry, your charge couldn't be processed. Reason: ".$error['message'];
} catch (\Stripe\Error\Card $e) {
  // Card was declined.
  $e_json = $e->getJsonBody();
  $error = $e_json['error'];
  echo "Sorry, your charge couldn't be processed. Reason: ".$error['message'];
}

?>
