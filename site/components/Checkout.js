import React from "react";
import $ from 'jquery';

export default class Checkout extends React.Component{

  constructor(){
    super();
    var submitBtn = document.getElementById("submitBtn");
  };

  componentWillReceiveProps(){
  };

  submitFormHandler(e){
    e.preventDefault();

    var ccNum = $('.card-number').val();
    var cvcNum = $('.card-cvc').val();
    var expMonth = $('.card-expiry-month').val();
    var expYear = $('.card-expiry-year').val();
    var first_name = $('.first-name').val();
    var email = $('.email').val();
    var price = ((Math.ceil(this.props.checkoutProductPrice)) * 100);
    var address_line1 = $('#address_line1').val();
    var address_line2 = $('#address_line2').val();
    var address_city = $('.address_city').val();
    var address_state = $('.address_state').val();
    var address_country = $('.address_country').val();
    var address_zip = $('.address_zip').val();
    var soldoutImgSrc = $('.soldoutImgSrc').val();
    var soldoutImgUrl = $('.soldoutImgUrl').val();
    var productName = this.props.checkoutProductName;
    $('#payment-error-copy').text("Processing... This may take a moment. Please wait.");
    $('#payment-errors').removeClass('warning').removeClass('success').addClass('normal');
    submitBtn.value = "Processing...";

    var error = false;
    var self = this;
    var reportError = function(msg) {
      // Show the error in the form:
      $('#payment-errors').removeClass('normal').addClass('warning');
      $('#payment-error-copy').text(msg).addClass('error').removeClass('inactive');
      buttonEnableHandler();
      return false;
    };//reportError

    var paymentSuccessful = function(soldoutImgSrc,token){
      buttonDisableHandler();
      var filteredCatalogue = self.props.filteredCatalogue;
      soldoutImgSrc == "" ? soldoutImgSrc = "./img/soldout.png" : null;
      soldoutImgUrl == "" ? soldoutImgUrl = "#" : null;
      self.props.postPaymentHandler(filteredCatalogue,soldoutImgSrc,soldoutImgUrl,token);
      submitBtn.value = "Payment Successful";
    };
    var buttonDisableHandler = function(msg){
      $('#submitBtn').attr('disabled', 'disabled');// Disable button click
      $('#payment-error-copy').text(msg).addClass('error').removeClass('inactive');
      $('#submitBtn').addClass("disabled");
      submitBtn.value = "Processing...";
    };
    var buttonEnableHandler = function(){
      $('#submitBtn').prop('disabled', false); // Re-enable the submit button
      $('#submitBtn').removeClass("disabled"); // remove disabled class
      submitBtn.value = "Submit Payment";
    };

    buttonDisableHandler();

    // Validate the number:
    if (!Stripe.card.validateCardNumber(ccNum)) {
      error = true;
      reportError('The credit card number appears to be invalid.');
    };
    // Validate the CVC:
    if (!Stripe.card.validateCVC(cvcNum)) {
      error = true;
      reportError('The CVC number appears to be invalid.');
    };
    // Validate the expiration:
    if (!Stripe.card.validateExpiry(expMonth, expYear)) {
      error = true;
      reportError('The expiration date appears to be invalid.');
    };
    // Validate the first_name:
    if (first_name == "" || first_name == null) {
      error = true;
      reportError('You have not entered a name');
    };
    // Validate the email:
    var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    if (testEmail.test(email)){
    } else {
      error = true;
      reportError('Your email is not valid.');
    };
    // Validate the address_line1:
    if (address_line1 == "" || address_line1 == null) {
      error = true;
      reportError('You have not entered an address.');
    };
    // Validate the address_city:
    if (address_city == "" || address_city == null) {
      error = true;
      reportError('You have not entered a city.');
    };
    // Validate the address_state:
    if (address_state == "" || address_state == null) {
      error = true;
      reportError('You have not entered a state.');
    };
    // Validate the address_zip:
    if (address_zip == "" || address_zip == null) {
      error = true;
      reportError('You have not entered a zip.');
    };
    // Validate the address_country:
    if (address_country == "" || address_country == null) {
      error = true;
      reportError('You have not entered a country.');
    };
    if (!error) {
        // Get the Stripe token:
        Stripe.card.createToken({
            number: $('.card-number').val(),
            cvc: $('.card-cvc').val(),
            exp_month: $('.card-expiry-month').val(),
            exp_year: $('.card-expiry-year').val(),
            name: $('.first-name').val()
        }, function(status, response){
            if (response.error) {
                var token = response.id;
                console.log("error code is: " + response);
                reportError(response.error.message);
                buttonEnableHandler();
                $('#payment-errors').removeClass('success').removeClass('normal').addClass('warning');
        		    buttonEnableHandler();
                var paymentFormAppender = $("#payment-form");
                paymentFormAppender.append($('<input id="" type="hidden" name="stripeToken">').val(token));
            } else { // No errors, submit the form.
              var paymentForm = $("#payment-form");
              var token = response.id;
              paymentForm.append($('<input id="" type="hidden" name="stripeToken">').val(token));
                $.ajax({
                   type: 'POST',
                   url: 'components/charge.php',
                   data : {
                     first_name: first_name,
                     email: email,
                     price: price,
                     address_line1: address_line1,
                     address_line2: address_line2,
                     address_city: address_city,
                     address_state: address_state,
                     address_country: address_country,
                     address_zip: address_zip,
                     productName: productName,
                     stripeToken: response.id
                   },
                   success: function(data,response,token) {
                     console.log("AJAX response is: " + response);
                     console.log("AJAX data is: " + data);
                     if(data == "success" || data.indexOf('success') != -1){
                       $('.formInput').val("");
                       $('#payment-errors').removeClass('normal').addClass('success');
                       $('#payment-error-copy').text("");
                       $('#payment-error-copy').text("Your payment was successful. Thank you for ordering!");
                       submitBtn.value = "Payment Successful";
                       paymentSuccessful(soldoutImgSrc,token);
                     } else {
                       $('#payment-errors').removeClass('normal').addClass('warning');
                       $('#payment-error-copy').text("");
                       $('#payment-error-copy').text(data);
                       buttonEnableHandler();
                     }
                   },
                   error: function(data,textStatus) {
                     console.log("Ajax Error!");
                     $('.formInput').val("");
                     $('#payment-errors').removeClass('normal').addClass('warning');
                     $('#payment-error-copy').text("");
                     $('#payment-error-copy').text("Apologies. No money has been charged to your account. A server issue occured so your information didn't get process. , you may want to try again.");
                     buttonEnableHandler();
                   }
                 });//$.ajax
            }//else
        });//Stripe.card.createToken
     }//!error
  };//submitFormHandler

  countryChangeHandler(){
    var country = $('.address_country').val();
    this.props.countryGlobalChangeHandler(country);
    console.log(country)
  };

  soldoutTooltipHandler(e){
    e.preventDefault();
    console.log("tooltip clicked");
    this.props.soldoutTooltipHandler();
  };

  render(){

    return(
      <div id="checkoutWindow" className={this.props.checkoutActive?"checkoutWindow modalBg catalogueFont" : "catalogueFont hidden"}>
        <div className={this.props.checkoutActive ? "checkoutModal modalWindow active" : "checkoutModal modalWindow"}>
          <div className="checkoutInner">
            <h2>Checkout / Commit to buys</h2>

            <br/>
            <p><strong>DVD Title:</strong> {this.props.checkoutProductName}</p>
            <img src={"https://stickermata.com/emporium/img/dvds/"+this.props.checkoutProductImage} className="checkoutProductImg"/>
            <p><strong>Price:</strong> {this.props.checkoutProductPrice == "2.4" ? "$" + this.props.checkoutProductPrice + 0 : "$"+this.props.checkoutProductPrice}
            </p>
            <p><strong>Conditon is:</strong> {this.props.checkoutProductCondition}</p>
            <p><strong>Stock:</strong>{this.props.checkoutPaymentProductStock}</p>
            <p><strong>Notes and Specs:</strong>{this.props.checkoutPaymentProductNotesAndSpecs}</p>

            <form id="payment-form" action="components/charge.php" method="POST" onSubmit={this.submitFormHandler.bind(this)}>

              <h3 className="checkout-subHeader">Customer Details</h3>

              <label>Name*</label>
              <input className="first-name full-width formInput" id="first_name" type="text" size="35" data-stripe="name" name="first_name"/>
              <br/>

              <label>Email*</label>
              <input className="email full-width formInput" id="email" type="email" size="35" data-stripe="email" name="email"/>
              <br/>

              <label>Promo image<span className="tooltip"><a id="soldoutTT" href="#" onClick={this.soldoutTooltipHandler.bind(this)}> What is this? {this.props.soldoutTooltipActive ? "X" : null}</a></span></label>
              <div className={this.props.soldoutTooltipActive ? "soldoutTTWindow" : ""}><span>
                {this.props.soldoutTooltipActive ?
                  <span>
                    - Replace the sold out image with an image for your business or website. <br/>
                    - Leave blank for default. <br/>
                    - Ideal specs: 241px x 241px, Anything bigger will simply look bad and run off the catalogue viewing area.<br/>
                    Inappropriate or insanely large images will be removed.(Max size 200kb)
                  </span>
                  :
                  null
                }
              </span></div>
              <input className="soldoutImgSrc formInput" type="text" />
              <br/>
              <label>Promo URL</label>
              <input className="soldoutImgUrl formInput" type="text" />
              <br/>

              <h3 className="checkout-subHeader">Shipping Address</h3>
              <label>Street No / Name*</label>
              <input className="address_line formInput full-width" id="address_line1" type="text" size="35"  data-stripe="address_line1" name="address_line1"/>
              <br/>
              <input className="address_line formInput full-width" id="address_line2" type="text" size="35" data-stripe="address_line2" name="address_line2"/>
              <br/>
              <label>City*</label>
              <input className="address_city formInput full-width" id="address_city" type="text" size="35" data-stripe="address_city" name="address_city"/>
              <br/>
              <label>State*</label>
              <input className="address_state formInput full-width" id="address_state" type="text" size="35" data-stripe="address_state" name="address_state"/>
              <br/>
              <label>Zip*</label><br/>
              <input className="address_zip formInput" id="address_zip" type="text" size="22" data-stripe="address_zip" name="address_zip"/>
              <br/>
              <label>Country*</label><br/>
              <select className="address_country full-width formInput" id="address_country" data-stripe="address_country" name="address_country" onChange={this.countryChangeHandler.bind(this)}>
              <option title='' value=''></option>
              	<option title='Australia' value='Australia'>Australia</option>
              	<option title='Canada' value='Canada'>Canada</option>
              	<option title='Denmark' value='Denmark'>Denmark</option>
              	<option title='Finland' value='Finland'>Finland</option>
              	<option title='France' value='France'>France</option>
              	<option title='Ireland' value='Ireland'>Ireland</option>
              	<option title='Japan' value='Japan'>Japan</option>
              	<option title='Norway' value='Norway'>Norway</option>
              	<option title='Singapore' value='Singapore'>Singapore</option>
              	<option title='Spain' value='Spain'>Spain</option>
              	<option title='Sweden' value='Sweden'>Sweden</option>
              	<option title='United Kingdom' value='United Kingdom'>United Kingdom</option>
              	<option title='United States' value='United States'>United States</option>
              	<option title='Austria' value='Austria'>Austria</option>
              	<option title='Belgium' value='Belgium'>Belgium</option>
              	<option title='Germany' value='Germany'>Germany</option>
              	<option title='Hong Kong' value='Hong Kong'>Hong Kong</option>
              	<option title='Italy' value='Italy'>Italy</option>
              	<option title='Luxembourg' value='Luxembourg'>Luxembourg</option>
              	<option title='Netherlands' value='Netherlands'>Netherlands</option>
              	<option title='New Zealand' value='New Zealand'>New Zealand</option>
              	<option title='Portugal' value='Portugal'>Portugal</option>
              	<option title='Switzerland' value='Switzerland'>Switzerland</option>
              </select>
              <br/>

              <h3 className="checkout-subHeader">Payment Details</h3>

              <label>Card Number <span className="tooltip">(Numbers only)</span></label><br/>
              <input className="card-number formInput" type="text" size="20" autoComplete="off" placeholder="No hyphens or spaces." data-stripe="number"/>
              <br/>

              <label>CVC </label>
              <input className="card-cvc formInput" type="text" size="4" autoComplete="off" data-stripe="cvc"/>
              <br/>

              <label>Expiration (MM/YYYY) </label>
              <input className="card-expiry-month formInput" type="text" size="2" data-stripe="exp_month"/>
              <span> / </span>
              <input className="card-expiry-year formInput" type="text" size="4" data-stripe="exp_year"/>

              <br/>
              <input type="submit" id="submitBtn" className="submit payBtn" value="Submit Payment" />

            </form>

            <div id="payment-errors" className="payment-errors">
              <p id="payment-error-copy" className="payment-error-copy inactive"></p>
            </div>

            <button id="checkoutClose" className="closeBtn catalogueFont" onClick={this.props.checkoutCloseClickhandler.bind(this)}> X</button>
          </div>
        </div>
      </div>
    )
  }

}
