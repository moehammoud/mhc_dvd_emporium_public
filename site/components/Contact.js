import React from "react";
import ReactDOM from "react-dom";
import $ from 'jquery';

export default class Contact extends React.Component{

  contactFormSubmitHandler(e){
    e.preventDefault();

    var contact_name = $('.contact_name').val();
    var contact_email = $('.contact_email').val();
    var contact_subject = $('.contact_subject').val();
    var contact_reference = $('.contact_reference').val();
    var contact_message = $('.contact_message').val();

    var error = false;
    var reportError = function(msg) {
      $('#contactAlert').removeClass('normal').addClass('warning');
      $('#contactAlert-copy').text(msg).addClass('error').removeClass('inactive');
      buttonEnableHandler();
      return false;
    };//reportError

    var buttonDisableHandler = function(msg){
      $('#contactSubmitBtn').attr('disabled', 'disabled');// Disable buttong click
      $('#contactAlert').text(msg).addClass('error').removeClass('inactive');
      $('#contactSubmitBtn').addClass("disabled");
    };
    var buttonEnableHandler = function(){
      $('#contactSubmitBtn').prop('disabled', false); // Re-enable the submit button
      $('#contactSubmitBtn').removeClass("disabled"); // remove disabled class
    };



    buttonDisableHandler();

    if(contact_name == "" || contact_name == null){
      error = true;
      reportError('*A name is required.');
    };

    var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    if (testEmail.test(contact_email)){
    } else {
      error = true;
      reportError('*A valid email is required.');
    };

    if(contact_subject == "" || contact_subject == null){
      error = true;
      reportError('*A subject is required.');
    };

    if(contact_message == "" || contact_message == null){
      error = true;
      reportError('*A message is required');
    };

    if(error == false){
      console.log("contact form submitted");
      $('#contactAlert').removeClass('warning').removeClass('success').addClass('normal');
      $('#contactAlert-copy').text("Processing...");

      $.ajax({
         type: 'POST',
         url: 'components/contact_send.php',
         data : {
           contact_name : contact_name,
           contact_email : contact_email,
           contact_subject : contact_subject,
           contact_message : contact_message,
           brandEmail : this.props.contactData.email
         },
         success: function(data,response) {
           console.log("AJAX response is: " + response);
           console.log("AJAX data is: " + data);
           $('#contactAlert').removeClass('normal').addClass('success');
           $('#contactAlert-copy').text("Your email has been sent sucessfully. I will be in touch with you shortly.");
           buttonDisableHandler();
         },
         error: function(data,textStatus) {
           console.log("Ajax Error!");
           $('#payment-errors').removeClass('normal').addClass('warning');
           $('#contactAlert-copy').text("Apologies. The email did not send through successfully. You may want to try again in a few moments.");
           buttonEnableHandler();
         }
       });//$.ajax

    };//if error == false (no errors)


  };

  //Close Window Handler
  closeWindowHandler(){
    $('.contactFormInput').val("");
    $('#contactSubmitBtn').prop('disabled', false); // Re-enable the submit button
    $('#contactSubmitBtn').removeClass("disabled"); // remove disabled class
    this.props.contactWindowToggle();
  };

  render(){

    return(
      <div id="" className={this.props.contactActive ? "modalBg mainFontLarge" : "modalBg mainFontLarge hidden"}>
        <div id="contact" className={this.props.contactActive ? "modalWindow returnActive" : "modalWindow hidden"}>
          <h1>Contact Me</h1>
          <p className="catalogueFont">
          Please feel free to contact me about anything you need. I will handle refunds and questions through this email.<br/><br/>
            <strong>Email:</strong> <a href={"mailto:"+this.props.contactData.email}>{this.props.contactData.email}</a> <br/>
            <strong>Mobile:</strong> <a href={"tel:"+this.props.contactData.phone}>{this.props.contactData.phone}</a> <br/>
            <strong>Base:</strong> {this.props.contactData.base} <br/>
          </p>

          <br/>

          <div id="contactAlert" className="contactAlert catalogueFont">
            <p id="contactAlert-copy" className="contactAlert-copy inactive"></p>
          </div>

          <br/>

          <form id="contact-form" action="components/contact_send.php" method="POST" onSubmit={this.contactFormSubmitHandler.bind(this)}>

            <h3 className="checkout-subHeader">Contact</h3>

            <label>Name*</label><br/>
            <input className="contact_name full-width contactFormInput" id="first_name" type="text" size="35" name="contact_name"/>
            <br/>

            <label>Email*</label><br/>
            <input className="contact_email full-width contactFormInput" id="contact_email" type="text"  name="contact_email"/>
            <br/>

            <label>Subject*</label><br/>
            <input className="contact_subject full-width contactFormInput" id="contact_subject" type="text"  name="contact_subject"/>
            <br/>

            <label>Reference</label><br/>
            <input className="contact_reference full-width contactFormInput" id="contact_reference" type="text"  name="contact_reference" placeholder="optional"/>
            <br/>

            <label>Message*</label><br/>
            <textarea className="contact_message full-width contactFormInput" id="contact_message" type="text"  name="contact_message" rows="4" cols="50"></textarea>
            <br/>

            <input type="submit" id="contactSubmitBtn" className="send payBtn" value="Send Message" name="submit"/>

          </form>


          <button id="returnClose" className="closeBtn" onClick={this.closeWindowHandler.bind(this)}>X</button>
        </div>
      </div>
    )
  }

}
