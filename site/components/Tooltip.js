import React from "react";
import ReactDOM from "react-dom";

export default class About extends React.Component{

  render(){

    return(
      <div id="about" className={this.props.aboutActive ? "modalBg" : "hidden"}>
        <div id="aboutModal" className={this.props.aboutActive ? "modalWindow active" : "hidden"}>
          <h1 className="mainFontLarge">What is this?</h1>
          <p>
            This website was built to raise money.
            <br/><br/>

            Initially, I wanted to sell designs, then I reconsidered and thought something on a smaller scale would be a better start.
            <br/><br/>

            I thought selling my DVDs would be a good idea and thought instead of doing it through ebay, I could do it myself as an exercise and as a canvas to showcase my work to aspiring devs to learn from.
            <br/><br/>

            When I'm not doing my 9-5, I am doing this sort of work and other sorts outside work hours. I want to use my front end dev skills to teach others.
            <br/><br/>

            I want to teach beginners and anyone keen to learn about the fundamentals of coding and in particular building in React and Webpack. Eventually progressing the tutorials to the stage that users can build a site like this one. I'll be starting a YouTube tutorial series on Webpack and React soon.<br/>
            My YouTube channel with current tutorials and more to come soon.<br/>
            <a href="https://www.youtube.com/channel/UC7LAcHoSCZ00RwU-pJW4B4w">mhcreative</a>
            <br/><br/>

            If you want to learn more or ask a question, you can contact me using the contact form or email me at:<br/>
            <a href={"mailto:"+this.props.contactData.email}>{this.props.contactData.email}</a>
            <br/><br/>

            <strong>This website's specs:</strong><br/>

            This website was built in <strong>React</strong> and componentised through <strong>webpack</strong>. The content is pull through using <strong>Firebase</strong>.<br/><br/>

            The form validation is done through <strong>jQuery</strong> although it can be done through vanilla JavaScript.<br/><br/>

            The handling of the purchase is done with <strong>Stripe.js</strong> and the php is processed with an ajax call.<br/><br/>
          </p>

          <button id="aboutClose" className="closeBtn" onClick={this.props.aboutWindowToggle.bind(this)}>X</button>
        </div>
      </div>
    )
  }

}
