import React from "react";
import ReactDOM from "react-dom";
import firebase from 'firebase';

import Catalogue from './Catalogue.js';
import Checkout from './Checkout.js';
import SearchFilter from './SearchFilter.js';
import About from './About.js';
import Pricing from './Pricing.js';
import Contact from './Contact.js';

export default class Front extends React.Component {

  constructor(){
    super();
    this.state = {
      jsonData : [],
      pricingData : [],
      postageMultiData : [],
      postageSingleData : [],
      contactData : [],
      DVDLibrary : [],
      pageLoaded:false,
      checkoutProductName: "placeholder",
      checkoutProductImage: "soldout_1.png",
      checkoutProductPrice: 8,
      checkoutProductPriceStatic:8,
      checkoutProductCondition: "Conditions apply",
      checkoutPaymentProductPrice: 2,
      checkoutPaymentProductStock:99,
      checkoutPaymentProductID:987,
      checkoutPaymentProductNotesAndSpecs: "single dvd",
      checkoutProductMultiDVD:"FALSE" ,
      aboutCopy:"Loading... Won't be long now.",
      pricingCopy:"Loading... Won't be long now.",
      returnPolicyCopy:"Loading... Won't be long now.",
      soldOutImage: "./img/soldout.png",
      soldoutURL: "https://stickermata.com/emporium",
      productIndex:null,
      checkoutActive: false,
      aboutActive: false,
      returnPolicyActive:false,
      pricingActive:false,
      contactActive:false,
      soldoutTooltipActive: false,
      searchFilterInput: "",
      filteredCatalogue: []
    };
    const self=this;
    firebase.database().ref().on('value',snapShot => {
      this.setState({
        DVDLibrary: snapShot.val().dvds,
        aboutCopy: snapShot.val().about,
        returnPolicyCopy: snapShot.val().policy,
        pricingData : snapShot.val().pricing,
        postageSingleData : snapShot.val().postage.single_DVD,
        postageMultiData : snapShot.val().postage.multi_DVD,
        contactData : snapShot.val().contact
      });
      if(self.state.pageLoaded == false){
        self.catalogueLoadInit();
      }
    });
  };//constructor


  // Load Catalogue
  catalogueLoadInit(){
    this.setState({
      pageLoaded:true
    })
  }

  // DVD checkout details update
  checkoutproductUpdateHandler(index,filteredCat){
    this.setState({
      checkoutActive:true,
      checkoutProductName: filteredCat[index].title,
      checkoutProductImage: filteredCat[index].image,
      checkoutProductPrice: filteredCat[index].price,
      checkoutProductPriceStatic: filteredCat[index].price,
      checkoutPaymentProductPrice: (filteredCat[index].price * 100),
      checkoutProductCondition: filteredCat[index].condition,
      checkoutPaymentProductStock: filteredCat[index].stock,
      checkoutPaymentProductID: filteredCat[index].id,
      checkoutPaymentProductNotesAndSpecs: filteredCat[index].NotesAndSpecs,
      productIndex:index,
      filteredCatalogue:filteredCat,
      checkoutProductMultiDVD:filteredCat[index].multi_dvds
    });
  };

  // Open soldout URL
  productSoldoutClickHandler(index,filteredCat){
    var soldoutURLVar = filteredCat[index].soldoutURL;
    this.setState({
      soldoutURL:soldoutURLVar
    });
    if(soldoutURLVar != "#" ){
      window.open(filteredCat[index].soldoutURL);
    };
  };

  // About Window Toggle Handler
  aboutWindowToggle(){
    this.setState({
      aboutActive : !this.state.aboutActive
    });
  }

  // Pricing Handler
  pricingWindowToggle(){
    this.setState({
      pricingActive : !this.state.pricingActive
    });
  };

  // Contact window handler
  contactWindowToggle(){
    this.setState({
      contactActive : !this.state.contactActive
    });
  };

  // Tooltip Toggle handler
  soldoutTooltipHandler(){
    this.setState({
      soldoutTooltipActive : !this.state.soldoutTooltipActive
    });
  };

  // Checkout close Handler
  checkoutCloseClickhandler(){
    this.setState({checkoutActive:false});
    document.getElementById("payment-error-copy").innerHTML = "";
    document.getElementById("payment-error-copy").classList.add('inactive');
    document.getElementById("payment-errors").classList.remove("success");
    document.getElementById("submitBtn").classList.remove("disabled");
    document.getElementById("submitBtn").disabled = false;
    var elements = document.getElementsByTagName('select');
    for (var i = 0; i < elements.length; i++)
    {
        elements[i].selectedIndex = 0;
    };
  };

  // Search Filter handler
  searchFilterInputHandler(input){
    this.setState({
      searchFilterInput: input
    });
  };

  // Post Payment Handler
  postPaymentHandler(filteredCatalogue,soldoutImgSrc,soldoutImgUrl,token){

    var URLSplit;
    var newURL;
    var dvdIndex = this.state.productIndex;
    var self = this;
    var tokenUpdate = token;

    firebase.auth().signInAnonymously().catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log(errorCode);
      console.log(errorMessage);
      // ...
    });

    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        // User is signed in.
        var isAnonymous = user.isAnonymous;
        var uid = user.uid;

        console.log("isAnonymous: " + isAnonymous);
        console.log("uid: " + uid);

        if(soldoutImgUrl.indexOf("www.") == 0){
          URLSplit = soldoutImgUrl.split("www.");
          newURL = "http://" + URLSplit[1];
        } else {
          newURL = soldoutImgUrl;
        };

        if(self.state.DVDLibrary[dvdIndex].stock > 0){
          firebase.database().ref('dvds/'+self.state.checkoutPaymentProductID).update({
            stock : self.state.DVDLibrary[self.state.checkoutPaymentProductID].stock - 1,
            soldoutImage: soldoutImgSrc,
            soldoutURL : newURL,
            token : "test"
          });
        };

      } else {
        // User is signed out.
        // ...
      }
      // ...
  });//OnAuthStateChanged
};//PostPaymentHandler

componentWillReceiveProps(){
};

  // Calculating postage
  countryGlobalChangeHandler(country){

    if(country == "" || country == null){
      this.setState({checkoutProductPrice: "calculating..."});
      console.log(country);
    };

    if(this.state.checkoutProductMultiDVD == "TRUE" || this.state.checkoutProductMultiDVD == "SEASON" ){
      if(country == "Australia"){
        console.log(this.state.checkoutProductPriceStatic);
        console.log(this.state.postageMultiData.australia);
        this.setState({
          checkoutProductPrice: Math.ceil(this.state.checkoutProductPriceStatic + this.state.postageMultiData.australia)
        });
      } else if(country == "New Zealand"){
        this.setState({
          checkoutProductPrice: Math.ceil(this.state.checkoutProductPriceStatic + this.state.postageMultiData.NZ)
        });
      } else if(country == "Japan" || country == "Singapore" || country == "Hong Kong" || country == "Japan"){
        this.setState({
          checkoutProductPrice: Math.ceil(this.state.checkoutProductPriceStatic + this.state.postageMultiData.asia_pacific)
        });
      } else {
        this.setState({
          checkoutProductPrice: Math.ceil(this.state.checkoutProductPriceStatic + this.state.postageMultiData.rest_of_the_world)
        });
      };//check country
    };//if multiDVD is true

    if(this.state.checkoutProductMultiDVD == "FALSE"){
      if(country == "Australia"){
        this.setState({
          checkoutProductPrice: Math.ceil(this.state.checkoutProductPriceStatic + this.state.postageSingleData.australia)
        });
      } else if(country == "New Zealand"){
        this.setState({
          checkoutProductPrice: Math.ceil(this.state.checkoutProductPriceStatic + this.state.postageSingleData.NZ)
        });
      } else if(country == "Japan" || country == "Singapore" || country == "Hong Kong" || country == "Japan"){
        this.setState({
          checkoutProductPrice: Math.ceil(this.state.checkoutProductPriceStatic + this.state.postageSingleData.asia_pacific)
        });
      } else {
        this.setState({
          checkoutProductPrice: Math.ceil(this.state.checkoutProductPriceStatic + this.state.postageSingleData.rest_of_the_world)
        });
      }//Check country

    };//if multiDVD is true
    // <h4 className="sub donateLink">DVDonation</h4>

  };//countryGlobalChangeHandler



  render(){
    return(
      <div className="shopFront">

      <div className="shopFrontHeaderContainer">
        <div className="shopFrontHeader">
          <h1 className="siteTitle mainFontLarge">Moe&#39;s DVD Emporeum</h1>

          <div className="navMenu navFont">
            <h4 className="sub aboutLink" onClick={this.aboutWindowToggle.bind(this)}>What is this?</h4>
            <h4 className="sub pricingLink" onClick={this.pricingWindowToggle.bind(this)}>Pricing</h4>
            <h4 className="sub contactLink" onClick={this.contactWindowToggle.bind(this)}>Contact</h4>
          </div>

          <About
            aboutActive={this.state.aboutActive}
            aboutCopy={this.state.aboutCopy}
            contactData={this.state.contactData}
            aboutWindowToggle={this.aboutWindowToggle.bind(this)}
          />

          <Pricing
            pricingActive={this.state.pricingActive}
            pricingCopy={this.state.pricingCopy}
            pricingData={this.state.pricingData}
            postageSingleData={this.state.postageSingleData}
            postageMultiData={this.state.postageMultiData}
            pricingWindowToggle={this.pricingWindowToggle.bind(this)}
          />

          <Contact
            contactActive={this.state.contactActive}
            contactData={this.state.contactData}
            contactWindowToggle={this.contactWindowToggle.bind(this)}
          />

          <SearchFilter
            searchFilterInput={this.state.searchFilterInput}
            filteredCatalogue={this.state.filteredCatalogue}
            searchFilterInputHandler={this.searchFilterInputHandler.bind(this)}
          />

        </div>
      </div>

        <Catalogue
        DVDLibrary={this.state.DVDLibrary}
        searchFilterInput={this.state.searchFilterInput}
        filteredCatalogue={this.state.filteredCatalogue}
        checkoutproductUpdateHandler={this.checkoutproductUpdateHandler.bind(this)}
        searchFilterInputHandler={this.searchFilterInputHandler.bind(this)}
        productSoldoutClickHandler={this.productSoldoutClickHandler.bind(this)}
        />

        {
          this.state.pageLoaded ?
          <Checkout
          checkoutActive={this.state.checkoutActive}
          checkoutProductName={this.state.checkoutProductName}
          checkoutProductImage={this.state.checkoutProductImage}
          checkoutProductPrice={this.state.checkoutProductPrice}
          checkoutPaymentProductPrice={this.state.checkoutPaymentProductPrice}
          checkoutProductCondition={this.state.checkoutProductCondition}
          checkoutPaymentProductStock={this.state.checkoutPaymentProductStock}
          filteredCatalogue={this.state.filteredCatalogue}
          checkoutPaymentProductNotesAndSpecs={this.state.checkoutPaymentProductNotesAndSpecs}
          soldOutImage={this.state.soldOutImage}
          soldoutTooltipActive={this.state.soldoutTooltipActive}
          checkoutCloseClickhandler={this.checkoutCloseClickhandler.bind(this)}
          postPaymentHandler={this.postPaymentHandler.bind(this)}
          countryGlobalChangeHandler={this.countryGlobalChangeHandler.bind(this)}
          soldoutTooltipHandler={this.soldoutTooltipHandler.bind(this)}
          />
          :
          <img className="preloader" src="./img/preloader.gif" />
        }

      </div>
    );
  }
};
