import React from "react";
import ReactDOM from "react-dom";

export default class SearchFilter extends React.Component{

  // Search Filter Input handler
  searchFilterInputChange(e){
    let input = e.target.value.toLowerCase();
    console.log(input);
    this.props.searchFilterInputHandler(input);
  }

  render(){

    return(
      <div id="searchFilter" className="searchFilter navFont">
      Filter:
      <input className="filterInputField navFont" type="text" placeholder="Filter dvds" onChange={this.searchFilterInputChange.bind(this)} defaultValue={this.props.searchFilterInput}/>
      </div>
    )
  }

}
