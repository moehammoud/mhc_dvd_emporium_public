import React from "react";
import ReactDOM from "react-dom";

export default class Catalogue extends React.Component{

  productClickHandler(index,filteredCat){
    this.props.checkoutproductUpdateHandler(index,filteredCat);
  };

  soldOutClickHandler(index,filteredCat,e){
    e.preventDefault();
    this.props.productSoldoutClickHandler(index,filteredCat);
  };

  render(){

    let filteredCat = this.props.DVDLibrary.filter(
      (dvds) => {
        return(
          dvds.title.toLowerCase().indexOf(this.props.searchFilterInput) !== -1
        )
      }
    );

    var catologueList = filteredCat.map((dvds,index) => {
        return(
          <li key={dvds.id} className={dvds.stock < 1 ? "catItem catalogueFont soldout" : "catItem catalogueFont"} onClick={dvds.stock < 1 ? this.soldOutClickHandler.bind(this,index,filteredCat) : this.productClickHandler.bind(this,index,filteredCat)}>
            <div className="inner">
              <h3 className="dvdTitle">{dvds.title}</h3>

              <img src={"https://stickermata.com/emporium/img/dvds/"+dvds.image}/>
              {dvds.stock < 1 ? <div className="soldoutImg"><img src={dvds.soldoutImage} /></div> : null}

              <p className="catPrice"><strong>Price</strong>: {dvds.price == "" ? dvds.price + 0 : "$" + dvds.price } <br/>+ Postage and Handling</p>

              <p><strong>Condition</strong>: {dvds.condition}</p>

              <p><strong>Stock</strong>: {dvds.stock}</p>

              <p className="notesAndSpecs"><strong>Notes & Specs</strong>: {dvds.NotesAndSpecs}</p>

              <button className={dvds.stock < 1 ? "payBtn catalogueFont disabled" : "payBtn catalogueFont" }>{dvds.stock < 1 ? "Sold Out" : "Checkout" }</button>

            </div>
          </li>
        )
    });

    return(
      <div className="catalogueContent">
        <ul>
          {catologueList}
        </ul>
      </div>
    )
  }

}
