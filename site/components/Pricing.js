import React from "react";
import ReactDOM from "react-dom";

export default class Pricing extends React.Component{

  render(){

    return(
      <div id="" className={this.props.pricingActive ? "modalBg mainFontLarge" : "modalBg mainFontLarge hidden"}>
        <div id="returnPolicy" className={this.props.pricingActive ? "modalWindow returnActive" : "modalWindow hidden"}>
          <h1>Pricing</h1>
          <p className="catalogueFont">
            In order to make a small profit I round up all the total cost to the nearest dollar. For transparency sake, here is my pricing system.<br/><br/>

            <strong>Special DVD:</strong> {this.props.pricingData.special} <br/>
            <strong>Damaged / Unoriginal Case:</strong> {this.props.pricingData.case_issue}<br/>
            <strong>Single DVD:</strong> ${this.props.pricingData.single_dvd} <br/>
            <strong>DVD w/ Bonus DVD:</strong> ${this.props.pricingData.multi_dvd} <br/>
            <strong>Blu-Ray:</strong> ${this.props.pricingData.bluray} <br/>
            <strong>Entire season:</strong> ${this.props.pricingData.season} <br/><br/>

            In addition to the cost of the DVD, there is a price of shipping and handling. The price depends on where the DVD is being shipped to and the size and weight of the DVD being shipped.<br/><br/>
          </p>

          <table id="pricingTable">
            <tbody>

              <tr>
                <td></td>
                <td><strong>Australia</strong></td>
                <td><strong>New Zealand</strong></td>
                <td><strong>Asia / Pacific</strong></td>
                <td><strong>Rest of the World</strong></td>
              </tr>

              <tr>
                <td>Single DVD</td>
                <td>${this.props.postageSingleData.australia}</td>
                <td>${this.props.postageSingleData.NZ}</td>
                <td>${this.props.postageSingleData.asia_pacific}</td>
                <td>${this.props.postageSingleData.rest_of_the_world}</td>
              </tr>

              <tr>
                <td>Multiple DVDs</td>
                <td>${this.props.postageMultiData.australia}</td>
                <td>${this.props.postageMultiData.NZ}</td>
                <td>${this.props.postageMultiData.asia_pacific}</td>
                <td>${this.props.postageMultiData.rest_of_the_world}</td>
              </tr>

            </tbody>
          </table><br/>

          <p className="catalogueFont">
            If the cost of shipping changes during the order process, I will notify you and refund your money if neccessary. If anything goes wrong or you need to contact me, email me at <a href="mailto:moehammoud1@gmail.com">moehammoud1@gmail.com</a>
          </p>

          <button id="returnClose" className="closeBtn" onClick={this.props.pricingWindowToggle.bind(this)}>X</button>
        </div>
      </div>
    )
  }

}
