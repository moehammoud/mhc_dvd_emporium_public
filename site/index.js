import React from 'react';
import ReactDOM from 'react-dom';
var firebase = require('firebase');

var config = {
  apiKey: "YOUR_API_KEY",
  authDomain: "YOUR_DB.firebaseapp.com",
  databaseURL: "https://dvds-f421f.firebaseio.com"
};
firebase.initializeApp(config);

import Front from "./components/Front.js";

var siteFront = document.getElementById("siteFront");
ReactDOM.render(<Front />, siteFront);
