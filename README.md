# DVD Emporium Working files for the public #
A repository for DVD Emporium that the public can use for their own study and practice.  

- - - -

**This repo contains:**  

* React and React DOM  
* Babel  
* Firebase & Firebase library for React integration  
* Stripe.js  
* Webpack with webpack.config.js file setup for componentisation  
* Composer preinstalled for Stripe PHP library integration  
* jQuery(optional)

**What's missing from the build:**

* The Stripe secret key from **charge.php** file. 
* The Stripe publishable key from the **index.html** file.  
These keys are essential when integrating purchasing / payment functionality using stripe.
* The Firebase api key reference in the **index.js**  
The api key is essential for firebase DB integration.
- - - -

# How do I get set up? #

## Step1 ##
Download or Clone the repo into your local environment:

```
#!cmd

git clone https://moehammoud@bitbucket.org/moehammoud/mhc_dvd_emporium_public.git
```

- - - -

## Step 2 ##
With npm installed and your command line / terminal running enter.

```
#!npm

npm install
```

**The only extra library is jQuery which is optional. Everything else in the install package.json is essential.**  

- - - -

## Step 3 ##
With your command line/terminal open, cd to your local environment and run:

```
#!webpack

webpack --watch
```
**This will ensure that all the changes you make will be recompiled every time you save your work.**  
##optional##
You can use: 

```
#!webpack

webpack --dev-server
```
to host your work on but eventually you will need to run php on an apache based server. I would recommend using xampp/mampp to host your work and test PHP request when it comes to the payment functionality. 
- - - -

## Step 4 ##
That's it.

- - - -

### Contribution guidelines ###

There are none really. Play and use to your heart's content and hit me up with any suggestions or ideas you have. 

- - - -

### Who do I talk to? ###

* Contact me at moehammoud1@gmail.com or  
check out this build in action, [live](https://stickermata.com/emporium) and buy a DVD if you'd like to donate and promote your business / website using the sold out replacement feature on checkout.


### Special Thanks to: ###
[Larry Ullman](http://www.larryullman.com/series/processing-payments-with-stripe/) for his incredibly useful tutorials on how to integrate Stripe in an easy to understand way. 

[Josh Armstrong](http://joshivity.com/) for helping me with a basic understanding of AJAX. Kept me motivated with learning the payment functionality when I was considering giving up.